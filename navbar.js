import { useRef } from 'react';
import {FaBars , FaTimes} from 'react-icons/fa';
import { Link } from "react-router-dom"
import './Stylenav.css'

function Nav(){
    const navref= useRef();
    const affiche=()=>{
        navref.current.classList.toggle("resp");
    }
    return (
        <div className='header'>
        <img src='Ellipse 10.png' alt='' className='b'></img>
        <img src='LOGO.png' alt='' className='img' ></img>
        <div ref={navref} className='nav'>
            <Link to="/" className='link'>Accueil</Link>
            <Link to="/topics" className='link'>Notre services</Link>
           <Link to="/" className='link'>Annonce</Link>
            <Link to="/" className='link'>accueil</Link>
            <Link to="/" className='link ' ><button className='conx'>connecter</button></Link>
            <button className='btn-nav ferme' onClick={affiche}>
                <FaTimes /> </button>
        </div>
        <button className='btn-nav ' onClick={affiche}><FaBars /></button> 
        
        </div>
    )
}
export default Nav;  