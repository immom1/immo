import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../immomarche/annonce.css';
import Contact from '../immomarche/message';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const AnnonceDetails= () => {
  const { id } = useParams();
  const annonces = useSelector((state) => state.annonces);
  const annonce = annonces.find((a) => a.id === parseInt(id));

  const [selectedImage, setSelectedImage] = useState(annonce?.images[0]);
  const [showFeatures, setShowFeatures] = useState(false);

  const handlePrevClick = () => {
    const currentIndex = annonce?.images.indexOf(selectedImage) || 0;
    const newIndex = (currentIndex - 1 + (annonce?.images.length || 1)) % (annonce?.images.length || 1);
    setSelectedImage(annonce?.images[newIndex]);
  };

  const handleNextClick = () => {
    const currentIndex = annonce?.images.indexOf(selectedImage) || 0;
    const newIndex = (currentIndex + 1) % (annonce?.images.length || 1);
    setSelectedImage(annonce?.images[newIndex]);
  };

  const handleShowFeatures = () => {
    setShowFeatures(!showFeatures);
  };

  return (
    <div className='gallery'>
      {annonce ? (
        <>
          <div className='titre'>
            <h1 className="text-center">{annonce.title}</h1>
            <h1>{annonce.price}</h1>
          </div>
          <div className='gbl'>
            <div className="container mt-5">
              <div className='imag center'>
                <img
                  src={selectedImage}
                  alt={annonce.title}
                  className="img-fluid rounded"
                  onClick={() => setSelectedImage(annonce.images[0])}
                />
              </div>
              <div className="imag images">
                <button className="btns" onClick={handlePrevClick}>&lt;</button>
                {annonce.images.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    alt={`${index + 1}`}
                    className="annonce-image"
                  />
                ))}
                <button className="btns" onClick={handleNextClick}>&gt;</button>
              </div>
            </div>
            <div>
              <Contact contact={annonce.id_proprietaire} />
            </div>
          </div>
          <div className='carac'>
            <h3 className='TEXT'>Critique</h3>
            <h5 className='TEXT'>Caractéristiques</h5>
            <hr />
            <table>
              <tbody>
                <tr>
                  <td>Surface</td>
                  <td>Chauffage</td>
                  <td>Pièces</td>
                  <td>Chambres</td>
                  <td>Salle de bain</td>
                  <td>Salon</td>
                </tr>
                <tr>
                  <td>{annonce.caracteristiques.surface}m<sup>2</sup></td>
                  <td>{annonce.caracteristiques.chauffage}</td>
                  <td>{annonce.caracteristiques.pieces}</td>
                  <td>{annonce.caracteristiques.chambre}</td>
                  <td>{annonce.caracteristiques.salleDeBain}</td>
                  <td>{annonce.caracteristiques.salon}</td>
                </tr>
              </tbody>
            </table>
            <button className='voir TEXT' onClick={handleShowFeatures}>
              Voir plus de caractéristiques
            </button>
          </div>
          {showFeatures && (
            <div className='pluscarac'>
              <div className='disc'>
                <h3>Description :</h3>
                <h5>{annonce.description}</h5>
                <p>{annonce.description}</p>
              </div>
              <div className='accom'>
                <h3>Accompagnant :</h3>
                <div className='gri'>
                {annonce.caracteristiques.surface && <img src="/B1 (2).png" alt="B1" />}
        {annonce.caracteristiques.chauffage && <img src="/B2.png" alt="B2" />}
        {annonce.caracteristiques.pieces && <img src="/B3.png" alt="B3" />}
        {annonce.caracteristiques.chambre && <img src="/B4.png" alt="B4" />}
        {annonce.caracteristiques.salleDeBain && <img src="/B5.png" alt="B5" />}
        {annonce.caracteristiques.salon && <img src="/B6.png" alt="B6" />}
                </div>
              </div>
            </div>
          )}
        </>
      ) : (
        <p>Annonce non trouvée</p>
      )}
    </div>
  );
};

export default AnnonceDetails;





