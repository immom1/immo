import React, { useState } from 'react';
import './message.css'; 

function Contact({ ownerInfo }) {
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [description, setDescription] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Message envoyé:', { name, phone, email, description });
    };

    const handleCall = () => {
        console.log('Appel en cours...');
    };

    return (
        <div className="containers">
            <div className="owner-info">
                <img src={ownerInfo.image} alt="Owner" className="owner-image" />
                <h4 className="owner-name">{ownerInfo.name}</h4>
            </div>
            <div className="form-container">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input 
                            type="text" 
                            className="form-control" 
                            id="name" 
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            required 
                            placeholder='Nom'
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="tel" 
                            className="form-control" 
                            id="phone" 
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                            required 
                            placeholder='Téléphone'
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="email" 
                            className="form-control" 
                            id="email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required 
                            placeholder='Email'
                        />
                    </div>
                    <div className="form-group">
                        <textarea 
                            className="form-control" 
                            id="description" 
                            rows="3" 
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            required 
                            placeholder='Description'
                        ></textarea>
                    </div>
                    <div className='but'>
                        <button type="submit" className="btn  ">envoyer</button>
                        <button type="button" className="btn" onClick={handleCall}>Call</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Contact;


