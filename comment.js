
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './comment.css';

function ContactForm() {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(`Email: ${email}, Message: ${message}`);
        
        
        navigate('/Topics', { state: { email, message } });
        setEmail('');
        setMessage('');
    };

    return (
        <form className='contact-form' onSubmit={handleSubmit}>
            <label htmlFor='email'>Email:</label><br></br>
            <input
                type='email'
                placeholder='exemple@gmail.com'
                id='ema'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
            />
             <button type='submit'>Envoyer</button>
            <label htmlFor='message'>Message:</label>
            <textarea
                id='message'
                placeholder='Votre message ici...'
                rows='5'
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                required
            ></textarea>
           
        </form>
    );
}

export default ContactForm;

